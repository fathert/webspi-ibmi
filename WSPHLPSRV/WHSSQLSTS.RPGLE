**FREE
//********************************************************************************************
//
// Copyright (C) 2015-2017 Tim Fathers (tim@fathers.me.uk)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
//********************************************************************************************
ctl-opt
  copyright('WSSPQLSTS - Open Source www.apache.org/licenses/LICENSE-2.0, Tim Fathers 2017')
  datfmt(*ISO) datedit(*YMD/) timfmt(*ISO)
  debug(*INPUT: *DUMP) option(*NODEBUGIO: *SRCSTMT)
  nomain;

/include qrpgleinc,WSPHLPSRV

dcl-c QSQLMSG const('QSQLMSG   *LIBL     ');
dcl-c QCPFMSG const('QCPFMSG   *LIBL     ');

//*******************************************************************************************
// SQL_EMSQL_ChkSts (Check Embedded SQL Status)
//  This procedure is used to check the SQL status after an embedded
//  SQL statement. The "no error" and "no data" statuses are returned
//  directly and any other status is transformed into either a
//  diagnostic or escape message (depending on its type) and sent
//  to the caller of this procedure.
//
//  Example:
//    exec sql select * from GNGAFF00;
//    if SQL_EMSQL_ChkSts(SQLCOD: SQLSTT: SQLERM) = SQL_EMSQL_EOD;
//      // no data
//    endif;
//
//*******************************************************************************************
dcl-proc SQL_EMSQL_ChkSts export;
  dcl-pi *n like(*IN);      // <--*ON=EoD/*OFF=NoEr
    SQLCOD_ int(10) const;  // -->SQL Code
    SQLSTT_ char(5) const;  // -->SQL State
    SQLERM_ char(70) const;
  end-pi;

  // Define local variables.
  dcl-s msgId char(7);
  dcl-s msgTyp char(10);
  dcl-s stackOffset uns(5) inz(0); // Stack entry for msg
  dcl-s rtnCde like(*IN);

  select;
      when SQLSTT_ = '00000' or  // No error
           SQLSTT_ = *BLANK;
          rtnCde = SQL_EMSQL_NO_ERR;
      when SQLSTT_ = '02000';    // No data/end of file
          rtnCde = SQL_EMSQL_EOD;
      other; // Error/warning
          exsr ChkSts;
  endsl;

  return rtnCde;

  //*******************************************************************************************
  begsr ChkSts;
    if %subst(SQLSTT_: 1: 2) = '01';
         msgTyp = '*DIAG';    // Warning
     else;
         msgTyp = '*ESCAPE';  // Error
     endif;

     // Determine the message id from the SQL state code.
     evalr msgId = %editc(%abs(SQLCOD_): 'X');
     %subst(msgId: 1: 2) = 'SQ';
     if %subst(msgId: 3: 1) = '0';
         %subst(msgId: 3: 1) = 'L';
     endif;

     // If the SQLCODE was 0 then it cannot be mapped to an i5/OS
     // message so send a message with the SQL state only.
     if SQLCOD_ = 0;
         EXC_SndMsg('CPF9898':
                    msgTyp:
                    SQLSTT_:
                    QCPFMSG:
                    '*':
                    stackOffset);
     else;
         EXC_SndMsg(msgId:
                    msgTyp:
                    SQLERM_:
                    QSQLMSG:
                    '*':
                    stackOffset);
     endif;
  endsr;
end-proc;
//*******************************************************************************************



