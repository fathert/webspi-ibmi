![LogoColorTextBelow.jpeg](https://bitbucket.org/repo/5pxrEo/images/2313255042-LogoColorTextBelow.jpeg)

# README #

## What is WebSP? ##
**WebSP** is a slim, open-source framework that exposes stored procedures on a database server as web-services which can be called directly or used to form the basis of web or mobile applications. It was written primarily with the IBM i (also known as AS/400, iSeries, system i) in mind but is not specifically tied to any database engine. 

The framework comprises a JavaEE component that runs under [TomEE](https://tomee.apache.org/) (Tomcat with JavaEE extensions) and backend functions written in the language of the database server being accessed. This repository hosts the platform specific backend 
for DB2 for i (IBM i/iSeries), the Java layer can be found [here](https://bitbucket.org/fathert/webspi-java).

## How do I get set up? ##
The simplest way is to use an RDi project.

* Clone this repository to your PC
* Create an IBM i project in RDi using the downloaded source folder as the project folder
* Push the complete project source to your project library
* Compile the BUILD CL using CRTBNDCL
* Run the BUILD CL with the source and object library as parameters, for example `CALL [PRJLIB]/BUILD ('[SRCLIB]' '[OBJLIB]')`

Assuming you have the Java layer installed correctly, you can verify the installation is correct by visiting this URL:

`http://[YOUR_HOST]:[PORT]/webspi/sp/db2-i/wsp_sp_rslvproc`

This should return a JSON string describing the `WSP_SP_RslvProc`'s meta data, like this:


```
#!json

{
  "name": "wsp_sp_rslvproc",
  "schema": "TIM2",
  "returnValue": 0,
  "parms": {
    "PRCNAM_": {
      "index": 3,
      "typeName": "CHARACTER VARYING",
      "isArray": false,
      "typeId": 0,
      "length": 128,
      "scale": 0
    },
    "BASIC_": {
      "index": 4,
      "typeName": "CHARACTER",
      "isArray": false,
      "typeId": 0,
      "length": 1,
      "scale": 0
    },
    "NAME_": {
      "index": 2,
      "typeName": "CHARACTER VARYING",
      "isArray": false,
      "typeId": 0,
      "length": 128,
      "scale": 0
    },
    "SCHEMA_": {
      "index": 1,
      "typeName": "CHARACTER VARYING",
      "isArray": false,
      "typeId": 0,
      "length": 128,
      "scale": 0
    }
  }
}
```

### Prerequisites ###

* IBM i OS 7.1 or later

### Contribution guidelines ###
TBC.

### Who do I talk to? ###

* Tim Fathers (tim@fathers.me.uk)